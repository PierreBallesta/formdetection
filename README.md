# Pattern detection in Matlab.

This program finds the various occurences of a given pattern in a signal. It is based on the convolution function. In the following we will show an example with step detection from the signal of an accelerometer.


## Getting Started

You need to have Matlab installed to be able to use this program. 

Download the file _app\_detectiondesignal.mlappinstall_. Double click on it and click install on the Matlab prop window.  
You can test the program by downloading the files _signalS.csv_ (as the signal to test) et _signalP.csv_ (for the pattern).


### Use

Once the app installed, you can open it and it should show the following window:

![UIAdd](image2/UIMatlab.png)  
_Figure 1 This is the app window._

Each case corresponds to the following:  

1. This is where you give the file where your pattern is saved. It should be saved as a .csv file with no header.  
2. Here you enter the file containing the signal to be studied. It should be saved as a .csv file with no header.  
3. Here you enter the minimum, maximum lengths the pattern may take and the number of lengths you want to test in between.  
4. Here you can add an estimation of the standard deviation of the noise. If you don't know, just put this value at 0.  
5. Here is the minimum value of the standard deviation of the signal on a studied length needed for the calculation to take place. It should ideally be at least twice the noise threshold.  
6. This gives the minimal precision you want to have in your data. The closest to 1 the better. Value below 0.8 can be considered to be of poor precision.  
7. How the length distribution should behave, with value spaced linearly, logarithmically, or in between.
8. The results are given here.  
9. The results are displayed here superimposed with the initial signal.  
10. Where should the results be saved? These are saved as .csv files.  
11. If an error takes place, a message will appear here.  
12. Once everything is filled, press the start button to begin.  


![Pattern](image2/fig1_cardinal.png?raw=true)  
_Figure 2 The pattern we're looking for (here sin(x)/x). We will look for all occurences of this pattern in the following signal._

![Signal](image2/fig2_cardinal.png?raw=true)  
_Figure 3 The signal we're studying that contains some pattern of various lengths and amplitudes._

![Signal](image2/fig3_cardinal.png?raw=true)
_Figure 4 Signal superimposed with the patterns. The colours make successive patterns easier to visualize._

### Exemple steps detection.

The following signal is the vertical acceleration obtained from an accelerometer attached to a leg during a walk.   
![Steps](image2/fig_Signal.png?raw=true)  
_Figure 5 Acceleration recorded._  
Note that the base value of our signal is negative because of gravity.

As you can see steps have a distinctive pattern. A model of this pattern is presented bellow.  
![Steps](image2/fig_Pattern.png?raw=true)  
_Figure 6 Model step._

We launch the program to look for steps in the signal with the parameter in the following image.
![Test1](image2/fig_test1.png?raw=true)   
_Figure 7 Test1._  

This results in no detection. The step between lengths (look at 3 and 11) is too small. This value is increased to 10 and a new test is launched.

We launch the program to look for steps in the signal with the parameters in the following image.
![Test2](image2/fig_test2.png?raw=true)   
_Figure 8 Test2, the circled part of the result is a false positive._  

This time too many signals are found. The minimal precision input (0.5) is too small and we got some false positives.  

We increase this value to a higher 0.87 and run another test.   
![Test3](image2/fig_test3.png?raw=true)   
_Figure 9 Test3._  

We only find a handfull of patterns, when others are obviously missed.

This Precision value is put at 0.82 and we run another test.   
![Test3](image2/fig_test4.png?raw=true)   
_Figure 10 Test4._  

Almost all of the steps are now found.

This result seems acceptable. We can save the results by entering a file name in (10) and pressing the save button.  
We end with a .csv file looking like what follows.

```
step_nbr  position length   amplitude	validity	 standard_deviation 

	0		336		102		0.625558	0.936839	0.355419  
	1		474		101		0.933554	0.966086	0.260439  
	2		611		 97		1.015524	0.973554	0.229982  
	3		747		 99		0.982912	0.993498	0.114033  
	4		885		100		1.034844	0.989390	0.145674  
	5		1022	104		0.977095	0.991725	0.128649  
	6		1160	97		1.026803	0.961893	0.276070  
	7		1300	103		1.029255	0.996209	0.087073  
	8		1438	104		1.045947	0.992212	0.124800   
	9		1576	93		1.040383	0.936670	0.355895  

```
This file allows us to check the regularity of the steps, their amplitude, and their frequency. This way we can see the acceleration as the run progress, how the runner starts or ends to try to improve her/his run.

## Authors

* **Pierre Ballesta** -  [PierreBallesta](https://bitbucket.org/PierreBallesta)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.

